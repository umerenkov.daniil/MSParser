﻿namespace MSParser.models.sites
{
	public class ConfigSite
	{
		public string Name { get; set; }
		public string Currency { get; set; } = "RUB";
		public string Selector { get; set; }
		public string SecondarySelector { get; set; }
		public int Delivery { get; set; } = 0;
		public int Section { get; set; } = 1;
	}
}
