﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSParser.models.currencies
{
	public class ConfigCurrency
	{
		public string Name { get; set; }
		public decimal Value { get; set; }
	}
}
