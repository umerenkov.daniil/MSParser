﻿using System;
using MSParser.models.sites;

namespace MSParser.models
{
	public class ParseOutput
	{
		public Uri Link { get; set; }
		public ConfigSite Config { get; set; }
		public decimal Price { get; set; }
        public bool Aborted { get; set; }
	}
}
