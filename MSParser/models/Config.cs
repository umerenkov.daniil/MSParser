﻿using System.Collections.Generic;
using MSParser.models.currencies;
using MSParser.models.sites;

namespace MSParser.models
{
	public class Config
	{
		public List<ConfigCurrency> Currencies { get; set; }
		public List<ConfigSite> Sites { get; set; }
	}
}
