﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Css.Values;
using Microsoft.Office.Interop.Excel;
using MSParser.models;
using MSParser.models.sites;
using static System.Decimal;

namespace MSParser
{
	public class Sections
	{
		public ConfigSite[] LocalPrior { get; set; }
		public ConfigSite[] LocalDefault { get; set; }
		public ConfigSite[] Foreign { get; set; }
	}

	public class ExcelOutput
	{
		private const XlRgbColor yellow = XlRgbColor.rgbYellow;
		private const XlRgbColor red = XlRgbColor.rgbSalmon;
		private const XlRgbColor green = XlRgbColor.rgbLightGreen;
		private const XlRgbColor black = XlRgbColor.rgbBlack;
		private const XlRgbColor white = XlRgbColor.rgbWhite;
		private const string not = "Not found";

		private readonly int foreignLength;
		private readonly int priorLength;
		private readonly int defaultLength;

		private readonly string _savePath;
		private readonly Dictionary<string, List<ParseOutput>> _result;
		private readonly List<ConfigSite> _sites;
		private readonly int _resultCount;
		private readonly Sections _sections;

		public ExcelOutput(string savePath, Dictionary<string, List<ParseOutput>> result, List<ConfigSite> sites)
		{
			_savePath = savePath;
			_result = result;
			_sites = sites;
			_resultCount = _result.Count;
			_sections = new Sections
			{
				LocalPrior = _sites.Where(s => s.Delivery == 0 && s.Section == 0).ToArray(),
				LocalDefault = _sites.Where(s => s.Delivery == 0 && s.Section == 1).ToArray(),
				Foreign = _sites.Where(s => s.Delivery != 0).ToArray(),
			};

			foreignLength = _sections.Foreign.Length;
			priorLength = _sections.LocalPrior.Length;
			defaultLength = _sections.LocalDefault.Length;
		}

		public void Create()
		{
			var startExcel = DateTime.Now;

			var wb = MakeWorkBook();
			var ws = (Worksheet)wb.Worksheets.Item[1];

			SetNames(ws, 5, 2, 2, 2, 2, 3);
			SetBorders(ws, 5, 2, 2, 2, 2, 3);
			SetFirstColumns(ws);

			MainOutput(ws);

			ws.UsedRange.Font.Name = "Arial";
			ws.UsedRange.Font.Size = 9;
			ws.UsedRange.Font.Color = black;
			ws.UsedRange.Columns.AutoFit();
			ws.UsedRange.Cells.HorizontalAlignment = XlHAlign.xlHAlignRight;
			ws.Columns[2].WrapText = true;
			ws.Range[ws.Cells[2, 1], ws.Cells[_resultCount + 1, 1]].Cells.HorizontalAlignment = XlHAlign.xlHAlignLeft;
			ws.Columns.AutoFit();

			var endExcel = DateTime.Now;
			var timeDiff = endExcel - startExcel;
			var timeDiffInSecs = (int)timeDiff.TotalSeconds;
			Console.WriteLine($"Excel in {timeDiffInSecs}s");

			ws.SaveAs(_savePath);
			wb.Close();

			Console.WriteLine("done");
		}

		private void MainOutput(Worksheet ws)
		{
			var currentRow = 2;
			var currentShop = 1;
			foreach (var output in _result)
			{
				var pedalzoo = output.Value.FirstOrDefault(o => o.Config.Name.ToLower().Contains("pedalzoo.ru"));

				var minLocal = output.Value
					.Where(o => o.Config.Delivery == 0)
					.Select(x => ToInt32(x.Price))
					.Where(x => x != 0)
					.DefaultIfEmpty().Min();

				var minForeign = output.Value
					.Where(o => o.Config.Delivery != 0)
					.Where(x => ToInt32(x.Price) != 0)
					.Select(x => ToInt32(x.Price + x.Config.Delivery))
					.DefaultIfEmpty().Min();


				var pedalPrice = pedalzoo?.Price ?? 0;

				if (pedalzoo != null)
					ws.Hyperlinks.Add(ws.Cells[currentRow, 2], pedalzoo.Link.AbsoluteUri, Type.Missing, pedalzoo.Link.AbsoluteUri, "");

				ws.Cells[currentRow, 1] = output.Key;
				ws.Cells[currentRow, 2] = pedalPrice == 0 ? not : pedalPrice.ToString(CultureInfo.InvariantCulture);
				if (pedalPrice == 0) ws.Cells[currentRow, 2].Interior.Color = yellow; 
				

				ws.Cells[currentRow, 3] = minLocal != 0 ? minLocal.ToString() : not;
				if (pedalPrice != 0 && minLocal != 0)
					ws.Cells[currentRow, 3].Interior.Color = pedalPrice <= minLocal ? green : red;
				else ws.Cells[currentRow, 3].Interior.Color = minLocal == 0 ? yellow : white;

				ws.Cells[currentRow, 4] = minForeign != 0 ? minForeign.ToString() : not;
				if (pedalPrice != 0 && minForeign != 0)
					ws.Cells[currentRow, 4].Interior.Color = pedalPrice <= minForeign ? green : red;
				else ws.Cells[currentRow, 4].Interior.Color = minForeign == 0 ? yellow : white; ;


				foreach (var parse in output.Value.Where(v => !v.Config.Name.ToLower().Contains("pedalzoo.ru")).ToArray())
				{
					var id = GetIndex(parse.Config);
					if (id == 0) continue;

					ws.Hyperlinks.Add(ws.Cells[currentRow, id], parse.Link.AbsoluteUri, Type.Missing, parse.Link.AbsoluteUri, "");

					if (parse.Price == 0)
					{
						ws.Cells[currentRow, id] = not;
						ws.Cells[currentRow, id].Interior.Color = yellow;
						continue;
					}

					if (parse.Config.Delivery == 0)
					{
						if (pedalPrice != 0)
						{
							ws.Cells[currentRow, id].Interior.Color = pedalPrice <= parse.Price ? green : red;
							ws.Cells[currentRow, id + 1] = parse.Price - pedalPrice;
							ws.Hyperlinks.Add(ws.Cells[currentRow, id + 1], parse.Link.AbsoluteUri, Type.Missing, parse.Link.AbsoluteUri, "");
						}

						ws.Cells[currentRow, id] = parse.Price;
					}
					else
					{
						if (pedalPrice != 0)
						{
							ws.Cells[currentRow, id + 1].Interior.Color = pedalPrice <= parse.Price + parse.Config.Delivery ? green : red;
							ws.Cells[currentRow, id + 2] = parse.Price + parse.Config.Delivery - pedalPrice;
							ws.Hyperlinks.Add(ws.Cells[currentRow, id + 1], parse.Link.AbsoluteUri, Type.Missing, parse.Link.AbsoluteUri, "");
						}
						ws.Cells[currentRow, id] = parse.Price;
						ws.Cells[currentRow, id + 1] = parse.Price + parse.Config.Delivery;
					}
				}

				Console.WriteLine($"excel: {currentShop}/{_resultCount}");
				currentShop++;
				currentRow++;
			}
		}

		private int GetIndex(ConfigSite config)
		{
			ConfigSite[] array;
			int term = 0;
			int factor = 0;

			if (config.Delivery != 0)
			{
				array = _sections.Foreign;
				factor = 3;
				term = 10 + defaultLength * 2 + priorLength* 2;
			} 
			else if (config.Section == 0)
			{
				array = _sections.LocalPrior;
				factor = 2;
				term = 6;
			}
			else
			{
				array = _sections.LocalDefault;
				factor = 2;
				term = 8 + priorLength * 2;
			}

			var innerId = array.ToList().FindIndex(c => c.Name.ToLower() == config.Name.ToLower());
			if (innerId == -1) return 0;
			return innerId * factor + term;
		}

		private void SetFirstColumns(Worksheet ws)
		{
			ws.Cells[1, 1] = "Название товара";
			ws.Cells[1, 2] = "Цена pedalzoo";
			ws.Cells[1, 3] = "Мин. цена в РФ";
			ws.Cells[1, 4] = "Мин. цена загран";

			var pedalRange = ws.Range[ws.Cells[1, 2], ws.Cells[_resultCount + 1, 2]];
			pedalRange.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
			pedalRange.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
			pedalRange.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
			pedalRange.Borders[XlBordersIndex.xlEdgeLeft].Weight = XlBorderWeight.xlMedium;

			var pricesRange = ws.Range[ws.Cells[1, 4], ws.Cells[_resultCount + 1, 1]];
			pricesRange.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
			pricesRange.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;

			ws.Application.ActiveWindow.SplitColumn = 1;
			ws.Application.ActiveWindow.FreezePanes = true;

			ws.Application.ActiveWindow.SplitColumn = 2;
			ws.Application.ActiveWindow.FreezePanes = true;

			var nameRange = ws.Range[ws.Cells[1, 1], ws.Cells[1, 4]];
			nameRange.Borders.LineStyle = XlLineStyle.xlContinuous;
			nameRange.Borders.Weight = XlBorderWeight.xlMedium;

			Console.WriteLine("Default namings generated");
		}

		#region Borders
		private void SetBorders(Worksheet ws, int priorOffset, int defaultOffset, int foreignOffset, int priorLength, int defaultLength, int foreignLength)
		{
			var position = priorOffset + 1;
			var startRow = 1;
			var endRow = _resultCount + 1;

			position = RangeBorder(ws, position, priorLength, startRow, endRow, _sections.LocalPrior);
			position += defaultOffset;
			position = RangeBorder(ws, position, defaultLength, startRow, endRow, _sections.LocalDefault);
			position += foreignOffset;
			RangeBorder(ws, position, foreignLength, startRow, endRow, _sections.Foreign);

			Console.WriteLine("Border setted");
		}

		private int RangeBorder(Worksheet ws, int position, int offet, int startRow, int endRow, ConfigSite[] enumerable)
		{
			foreach (var item in enumerable)
			{
				var range = ws.Range[ws.Cells[startRow, position], ws.Cells[endRow, position]];
				range.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
				range.Borders[XlBordersIndex.xlEdgeLeft].Weight = XlBorderWeight.xlMedium;
				position += offet;
			}

			var outeRange = ws.Range[ws.Cells[startRow, position], ws.Cells[endRow, position]];
			outeRange.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
			outeRange.Borders[XlBordersIndex.xlEdgeLeft].Weight = XlBorderWeight.xlMedium;

			return position;
		}
		#endregion

		#region Names
		private void  SetNames(Worksheet ws, int priorOffset, int defaultOffset, int foreignOffset, int priorLength, int defaultLength, int foreignLength)
		{
			var position = priorOffset + 1;
			var row = 1;

			position = RangeName(ws, position, priorLength, row, _sections.LocalPrior.Select(p => p.Name).ToArray());
			position += defaultOffset;
			position = RangeName(ws, position, defaultLength, row, _sections.LocalDefault.Select(p => p.Name).ToArray());
			position += foreignOffset;
			RangeName(ws, position, foreignLength, row, _sections.Foreign.Select(p => p.Name).ToArray());

			Console.WriteLine("Names generated");
		}

		private int RangeName(Worksheet ws, int position, int offet, int row, string[] data)
		{
			foreach (var site in data)
			{
				var range = ws.Range[ws.Cells[row, position], ws.Cells[row, position + offet - 1]];
				range.Merge();
				range.Value = site.ToLower();
				range.Borders.LineStyle = XlLineStyle.xlContinuous;
				range.Borders.Weight = XlBorderWeight.xlMedium;
				position += offet;
			}

			return position;
		}
		#endregion

		public void Test()
		{
			Console.WriteLine("Creating excel");
            var wb = MakeWorkBook();
			var ws = (Worksheet)wb.Worksheets.Item[1];

			ws.Cells[1, 1] = "Название товара";
			ws.Cells[1, 2] = "Цена pedalzoo";
			ws.Cells[1, 3] = "Мин. цена в РФ";
			ws.Cells[1, 4] = "Мин. цена загран";

			ws.Application.ActiveWindow.SplitColumn = 1;
			ws.Application.ActiveWindow.FreezePanes = true;

			ws.Application.ActiveWindow.SplitColumn = 2;
			ws.Application.ActiveWindow.FreezePanes = true;
			Console.WriteLine("Transforming results");
			var shopList = GetAllListedShop(ws);
			FillBorders(ws, shopList, _result.Count);

			Console.WriteLine("Generating ranges");
			int row = 2;
			var length = _result.Count;
			var i = 1;
			foreach (var keyValue in _result)
			{
				ws.Cells[row, 1] = keyValue.Key;
				var pedal = keyValue.Value.FirstOrDefault(p => p.Link.Host.ToLower().Contains("pedalzoo.ru"));

				var foreignSequence = keyValue.Value.Where(x => x.Price != 0)
					.Where(x => x.Config.Delivery != 0)
					.Select(x => x.Price + x.Config.Delivery).ToArray();

				var minForeign = foreignSequence.Any() ? foreignSequence.Min() : Int32.MaxValue;

				var localSequence = keyValue.Value.Where(x => x.Price != 0)
					.Where(x => x.Config.Delivery == 0)
					.Select(x => x.Price).ToArray();

				var minLocal = localSequence.Any() ? localSequence.Min() : Int32.MaxValue;

				ws.Cells[row, 3] = minLocal == Int32.MaxValue ? "Not Found" : minLocal.ToString();
				ws.Cells[row, 3].Interior.Color = minLocal == Int32.MaxValue ? XlRgbColor.rgbYellow : minLocal < pedal?.Price ?
					XlRgbColor.rgbSalmon : XlRgbColor.rgbLightGreen;

				ws.Cells[row, 4] = minForeign == Int32.MaxValue ? "Not Found" : minForeign.ToString();
				ws.Cells[row, 4].Interior.Color = minForeign == Int32.MaxValue ? XlRgbColor.rgbYellow : minForeign < pedal?.Price ?
					XlRgbColor.rgbSalmon : XlRgbColor.rgbLightGreen;

				foreach (ParseOutput item in keyValue.Value)
				{
					var conf = item.Config;
					if (item.Link.Host.ToLower().Contains("pedalzoo.ru"))
					{
						if (item.Price == 0)
							ws.Cells[row, 2].Interior.Color = XlRgbColor.rgbYellow;
						ws.Cells[row, 2] = item.Price;
						ws.Hyperlinks.Add(ws.Cells[row, 2], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
						continue;
					}
					if (conf.Delivery == 0)
					{
						if (conf.Section == 0)
						{
							var col = shopList.Local0.FindIndex(e => e == item.Link.Host) * 2 + 6;
							ws.Cells[row, col] = item.Price;
							ws.Hyperlinks.Add(ws.Cells[row, col], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
							ws.Cells[row, col].Font.Color = XlRgbColor.rgbBlack;

							if (pedal != null)	
							{
								if (item.Price < pedal.Price)
									ws.Cells[row, col].Interior.Color = XlRgbColor.rgbSalmon;
								if (item.Price >= pedal.Price)
									ws.Cells[row, col].Interior.Color = XlRgbColor.rgbLightGreen;
								if (pedal.Price != 0)
								{
									ws.Cells[row, col + 1] = item.Price - pedal.Price;
									ws.Hyperlinks.Add(ws.Cells[row, col + 1], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
									ws.Cells[row, col + 1].Font.Color = XlRgbColor.rgbBlack;
								}
							}

							if (item.Price != 0) continue;
							ws.Cells[row, col].Interior.Color = XlRgbColor.rgbYellow;
							ws.Cells[row, col] = "Not found";
							ws.Cells[row, col + 1].Interior.Color = XlRgbColor.rgbYellow;
							ws.Cells[row, col + 1] = "Not found";
						}
						else if (conf.Section == 1)
						{
							var col = shopList.Local1.FindIndex(e => e == item.Link.Host) * 2 + 8 + shopList.Local0.Count * 2;
							ws.Cells[row, col] = item.Price;
							ws.Hyperlinks.Add(ws.Cells[row, col], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
							ws.Cells[row, col].Font.Color = XlRgbColor.rgbBlack;

							if (pedal != null)
							{
								if (item.Price < pedal.Price)
									ws.Cells[row, col].Interior.Color = XlRgbColor.rgbSalmon;
								if (item.Price >= pedal.Price)
									ws.Cells[row, col].Interior.Color = XlRgbColor.rgbLightGreen;
								if (pedal.Price != 0)
								{
									ws.Cells[row, col + 1] = item.Price - pedal.Price;
									ws.Hyperlinks.Add(ws.Cells[row, col + 1], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
									ws.Cells[row, col + 1].Font.Color = XlRgbColor.rgbBlack;
								}
							}

							if (item.Price != 0) continue;
							ws.Cells[row, col].Interior.Color = XlRgbColor.rgbYellow;
							ws.Cells[row, col] = "Not found";
							ws.Cells[row, col + 1].Interior.Color = XlRgbColor.rgbYellow;
							ws.Cells[row, col + 1] = "Not found";
						}
					}
					else
					{
						var sumPrice = item.Config.Delivery + item.Price;
						var col = shopList.Foreign.FindIndex(e => e == item.Link.Host) * 3 + 10 + shopList.Local0.Count * 2 + shopList.Local1.Count * 2;
						ws.Cells[row, col] = item.Price;
						ws.Hyperlinks.Add(ws.Cells[row, col], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
						ws.Cells[row, col].Font.Color = XlRgbColor.rgbBlack;

						ws.Cells[row, col + 1] = sumPrice;
						ws.Hyperlinks.Add(ws.Cells[row, col + 1], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
						ws.Cells[row, col + 1].Font.Color = XlRgbColor.rgbBlack;

						if (pedal.Price != 0)
						{
							ws.Cells[row, col + 2] = sumPrice - pedal?.Price;
							ws.Hyperlinks.Add(ws.Cells[row, col + 2], item.Link.AbsoluteUri, Type.Missing, item.Link.AbsoluteUri, "");
							ws.Cells[row, col + 2].Font.Color = XlRgbColor.rgbBlack;
						}

						if (pedal != null)
						{
							if (sumPrice < pedal.Price)
								ws.Cells[row, col + 1].Interior.Color = XlRgbColor.rgbSalmon;
							if (sumPrice >= pedal.Price)
								ws.Cells[row, col + 1].Interior.Color = XlRgbColor.rgbLightGreen;
						}

						if (item.Price != 0) continue;
						ws.Cells[row, col].Interior.Color = XlRgbColor.rgbYellow;
						ws.Cells[row, col + 1].Interior.Color = XlRgbColor.rgbYellow;
						ws.Cells[row, col] = "Not found";
					}
				}
				Console.WriteLine($"{i}/{length}");
				i++;
				row++;
			}
			Console.WriteLine("Borders created");

			ws.UsedRange.Font.Name = "Arial";
			ws.UsedRange.Font.Size = 9;
			Console.WriteLine("Font applied");

			ws.Columns[2].WrapText = true;

			ws.Columns.AutoFit();
			Console.WriteLine("Auto fit applied");

			Console.WriteLine("Excel created");

			ws.SaveAs(_savePath);
			wb.Close();
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("all done");
		}

		private ShopList GetAllListedShop(Worksheet ws)
		{
			var result = new ShopList();
			var many = _result.Values.SelectMany(s => s);
			var all = many.Select(x => x.Config).Distinct().Where(e => !e.Name.ToLower().Contains("pedalzoo.ru")).ToList();
			result.All = all.Select(c => c.Name).ToList();

			result.Local0 = all.Where(x => x.Delivery == 0 && x.Section == 0).Select(x => x.Name).ToList();
			result.Local1 = all.Where(x => x.Delivery == 0 && x.Section == 1).Select(x => x.Name).ToList();
			result.Foreign = all.Where(x => x.Delivery != 0).Select(x => x.Name).ToList();

			Console.WriteLine("Setting borders");
			SetHeaderBorders(new Range[] { ws.Cells[1, 3], ws.Cells[1, 4] });

			int col = 6;
			foreach (var item in result.Local0)
			{
				ws.Cells[1, col] = item;
				ws.Range[ws.Cells[1, col], ws.Cells[1, col + 1]].Merge();
				SetHeaderBorders(new Range[] { ws.Cells[1, col], ws.Cells[1, col + 1] });
				col += 2;
			}

			col += 2;
			foreach (var item in result.Local1)
			{
				ws.Cells[1, col] = item;
				ws.Range[ws.Cells[1, col], ws.Cells[1, col + 1]].Merge();
				SetHeaderBorders(new Range[] { ws.Cells[1, col], ws.Cells[1, col + 1] });
				col += 2;
			}

			col += 2;
			foreach (var item in result.Foreign)
			{
				ws.Cells[1, col] = item;
				ws.Range[ws.Cells[1, col], ws.Cells[1, col + 1]].Merge();
				ws.Range[ws.Cells[1, col], ws.Cells[1, col + 2]].Merge();

				SetHeaderBorders(new Range[] { ws.Cells[1, col], ws.Cells[1, col + 1], ws.Cells[1, col + 2] });

				col += 3;
			}

			return result;
		}

		private void SetHeaderBorders(Range[] cells)
		{
			foreach (var cell in cells)
			{
				Borders border = cell.Borders;
				border.LineStyle = XlLineStyle.xlContinuous;
				border.Weight = XlBorderWeight.xlMedium;
			}
		}

		private Workbook MakeWorkBook()
		{
			var xlApp = new
				Microsoft.Office.Interop.Excel.Application();
			if (xlApp == null)
				throw new ArgumentException("Excel is not properly installed");

			xlApp.StandardFont = "Arial";
			xlApp.StandardFontSize = 10;
			xlApp.ScreenUpdating = false;

			var xlWorkBook = xlApp.Workbooks.Add(System.Reflection.Missing.Value);
			return xlWorkBook;
		}
		private void FillBorders(Worksheet ws, ShopList shopList, int shopsLength)
		{
			var mins = new[] { 3, 2 };
			var loc0 = new[] { 6, shopList.Local0.Count * 2 };
			var loc1 = new[] { 8 + shopList.Local0.Count * 2, shopList.Local1.Count * 2 };
			var foreign = new[] { 10 + shopList.Local0.Count * 2 + shopList.Local1.Count * 2, shopList.Foreign.Count * 3 };

			Console.WriteLine($"Min border {DateTime.Now.ToShortTimeString()}");
			ProccedLocalBorders(mins[0], ws, mins[1], shopsLength);
			Console.WriteLine($"loc0 border {DateTime.Now.ToShortTimeString()}");
			ProccedLocalBorders(loc0[0], ws, loc0[1], shopsLength);
			Console.WriteLine($"loc1 border {DateTime.Now.ToShortTimeString()}");
			ProccedLocalBorders(loc1[0], ws, loc1[1], shopsLength);
			Console.WriteLine($"inner border {DateTime.Now.ToShortTimeString()}");

			var marginTop = 2;
			for (int col = 0; col < foreign[1]; col++)
			{
				for (int row = 0; row < shopsLength; row++)
				{
					if (col % 3 == 0)
					{
						ws.Cells[row + marginTop, col + foreign[0]].Borders[XlBordersIndex.xlEdgeLeft].Weight =
							XlBorderWeight.xlMedium;
						ws.Cells[row + marginTop, col + foreign[0]].Borders[XlBordersIndex.xlEdgeLeft].LineStyle =
							XlLineStyle.xlContinuous;
					}
					else if (col % 3 == 2)
					{
						ws.Cells[row + marginTop, col + foreign[0]].Borders[XlBordersIndex.xlEdgeRight].Weight =
							XlBorderWeight.xlMedium;
						ws.Cells[row + marginTop, col + foreign[0]].Borders[XlBordersIndex.xlEdgeRight].LineStyle =
							XlLineStyle.xlContinuous;
					}
				}
			}
		}

		private void ProccedLocalBorders(int marginLeft, Worksheet ws, int colLength, int shopsLength)
		{
			var marginTop = 2;
			for (int col = 0; col < colLength; col++)
			{
				for (int row = 0; row < shopsLength; row++)
				{
					if (col % 2 == 0)
					{
						ws.Cells[row + marginTop, col + marginLeft].Borders[XlBordersIndex.xlEdgeLeft].Weight =
							XlBorderWeight.xlMedium;
						ws.Cells[row + marginTop, col + marginLeft].Borders[XlBordersIndex.xlEdgeLeft].LineStyle =
							XlLineStyle.xlContinuous;
					}
					else
					{
						ws.Cells[row + marginTop, col + marginLeft].Borders[XlBordersIndex.xlEdgeRight].Weight =
							XlBorderWeight.xlMedium;
						ws.Cells[row + marginTop, col + marginLeft].Borders[XlBordersIndex.xlEdgeRight].LineStyle =
							XlLineStyle.xlContinuous;
					}
				}

				Console.WriteLine($"{col}/{colLength} {DateTime.Now.ToShortTimeString()}");
			}
		}
	}
}

class ShopList
{
	public List<string> Local0 { get; set; }
	public List<string> Local1 { get; set; }
	public List<string> Foreign { get; set; }
	public List<string> All { get; set; }
}