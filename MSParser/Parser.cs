﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Parser.Html;
using MSParser.models;
using MSParser.models.currencies;
using MSParser.models.sites;
using Newtonsoft.Json;
using static System.Decimal;

namespace MSParser
{
	public class Parser
	{
		private readonly IBrowsingContext _context;
		private readonly HtmlParser _parser;
        private Config _config;
		private Currencies _currencies;
		private Dictionary<string, Uri[]> _links;
		private Dictionary<string, List<ParseOutput>> _output;
		private string _savePath;
		private int _total;
        private readonly object _printLock;
        private readonly object _saveResultLock;
        private int _parsed;
        private Task _currenciesWait;

        public Parser()
		{
			_output = new Dictionary<string, List<ParseOutput>>();
			var angleSharpConfig = Configuration.Default.WithDefaultLoader();
			_context = BrowsingContext.New(angleSharpConfig);
			_parser = new HtmlParser();
            _printLock = new object();
            _saveResultLock = new object();
            _parsed = 0;
        }

		public void Run()
        {
            // Старт парса
            var parseStart = DateTime.Now;

            Console.WriteLine($"Start {parseStart.ToShortTimeString()}");
            // Запрос конфига
			LoadConfig();

            // Таск парсинга валют
            _currenciesWait = Task.Run(SetCurrencies);

            // Запрос файла ссылок
            LoadLinks();

            // Определение пути сохранения
            DefineSavePath();

            // Сколько всего сайтов будет спарсено
            _total = _links.Select(o => o.Value.Length).Sum();

            // Парсинг
            Process();
            
            // Репарсинг
            TryReparse();

            // Эксель
            Console.WriteLine($"Parse in {(int)(DateTime.Now - parseStart).TotalSeconds}s");
            var excel = new ExcelOutput(_savePath, _output, _config.Sites);
            excel.Create();
        }

        public void Process()
        {
            // Ждем завершения загрузки валют
            _currenciesWait.Wait();

            _output = _links.ToDictionary(x => x.Key, x => new List<ParseOutput>());

            // Плоский список ссылок
            var flatRequests = _links.SelectMany(x => x.Value.Select(u => new {uri = u, name = x.Key}));

            // Максимальное количество параллельных потоков. Управление созданием потоков отдаем системе, потому что лень. 
            Parallel.ForEach(flatRequests, (link, state) =>
            {
                // Нормализация
                var withOutWs = link.uri.Host.StartsWith("www.") ? link.uri.Host.Substring(4) : link.uri.Host;

                // Так себе решение, но ботлнек не создает 
                var config = _config.Sites.FirstOrDefault(c => c.Name.ToLower().Contains(withOutWs.ToLower()));

                if (config == null)
                {
                    Console.WriteLine($"Can't find config for {withOutWs}");
                    PrintStatus(true);
                    return;
                }

                // Парсим
                var parseTask = Parse(link.uri, config);

                // Сам по себе каждый блок синхронный чтобы точно отловить конец парсинга, поэтому ждем дедовским способом
                parseTask.Wait();

                PrintStatus(parseTask.Result.Aborted);

                // Блоки синхронные, но запущены асинхронно, так что лок
                lock (_saveResultLock)
                {
                    var success = _output.TryGetValue(link.name, out var results);
                    if (success) results.Add(parseTask.Result);
                }
            });
        }

        private void PrintStatus(bool aborted = false)
        {
            var extra = aborted ? " aborted" : string.Empty;
            lock (_printLock)
            {
                _parsed++;
                Console.WriteLine($"{_parsed}/{_total}{extra}");
            }
        }

		private void TryReparse()
        {
            var toReparse = _output.SelectMany(x => x.Value.Where(r => r.Aborted || r.Price == 0).Select(result => new {result, x.Key})).ToList();
            _total = toReparse.Count;
            _parsed = 0;

            Console.WriteLine("Reparsing empty results...");

            Parallel.ForEach(toReparse, new ParallelOptions{ MaxDegreeOfParallelism = 3},  data =>
            {
                using (var webClient = GetWebClient())
                {
                    try
                    {
                        var body = webClient.DownloadString(data.result.Link);
                        if (string.IsNullOrEmpty(body))
                        {
                            PrintStatus(true);
                            return;
                        }

                        var parsed = _parser.Parse(body);
                        if (parsed.Body == null || string.IsNullOrEmpty(parsed.Body.InnerHtml))
                        {
                            PrintStatus(true);
                            return;
                        }

                        var currencyMultiplicator = _currencies.Valute
                            .FirstOrDefault(c => c.Key.ToLower() == data.result.Config.Currency.ToLower()).Value.Value;

                        var selectors = new[]
                        {
                            data.result.Config.Selector,
                            data.result.Config.SecondarySelector
                        };

                        selectors = selectors.Where(s => !string.IsNullOrEmpty(s)).ToArray();

                        data.result.Price = GetMinPrice(parsed, currencyMultiplicator, selectors);
                        PrintStatus();
                    }
                    catch (Exception)
                    {
                        PrintStatus(true);
                    }
                }
            });
        }

		private async Task<ParseOutput> Parse(Uri uri, ConfigSite config)
		{
			var result = new ParseOutput {Link = uri, Config = config};

            var currencyMultiplicator = _currencies.Valute.FirstOrDefault(c => c.Key.ToLower() == config.Currency.ToLower()).Value.Value;

			var document = await TryGet(uri);
			if (document == null)
			{
                result.Aborted = true;
                return result;
			}

			var selectors = new[]
			{
				config.Selector, config.SecondarySelector
			};

			selectors = selectors.Where(s => !string.IsNullOrEmpty(s)).ToArray();

			result.Price = GetMinPrice(document, currencyMultiplicator, selectors);
            return result;
		}

		private async Task<IDocument> TryGet(Uri uri)
		{
			var isPedal = uri.Host.ToLower().Contains("pedalzoo");
			var count = 0;
			var doc = await GetDoc(uri);
			var docInvalid = doc?.Body == null || string.IsNullOrEmpty(doc.Body.InnerHtml);
			var tries = isPedal ? 5 : 1;
			while (docInvalid && count < tries)
			{
				doc = await GetDoc(uri);
				docInvalid = doc?.Body == null || string.IsNullOrEmpty(doc.Body.InnerHtml);
				count++;
			}

			if (docInvalid)
			{
				doc = await GetDocAngle(uri);
				docInvalid = doc?.Body == null || string.IsNullOrEmpty(doc.Body.InnerHtml);
			}

			return docInvalid ? null : doc;
		}

		private async Task<IDocument> GetDocAngle(Uri uri)
		{
			try
			{
				return await _context.OpenAsync(uri.AbsoluteUri);
			}
			catch (Exception e)
			{
				return null;
			}
		}

		private async Task<IDocument> GetDoc(Uri uri)
		{
			string html;
			using (var webClient = GetWebClient())
			{
				try
				{
					html = await webClient.DownloadStringTaskAsync(uri);
				}
				catch (Exception e)
				{
					return null;
				}
			}

			try
			{
				return await _parser.ParseAsync(html);
			}
			catch (Exception e)
			{
				return null;
			}
		}

		private decimal GetMinPrice(IDocument document, decimal currencyMultiplicator, string[] selectors)
		{
			var prices = new List<decimal>();

			foreach (var selector in selectors)
			{
				var price = "0";
				IHtmlCollection<IElement> nodes;

				try
				{
					nodes = document.Body.QuerySelectorAll(selector);
				}
				catch (Exception e)
				{
					Console.WriteLine($"{e.Message} {selector}");
					continue;
				}

				if (nodes.Length > 0) price = nodes.Select(n => n.TextContent).FirstOrDefault();
				var result = RemoveNonDigits(price);
				result *= currencyMultiplicator;
				if (result != 0) prices.Add(result);
			}

			try
			{
				var min = prices.DefaultIfEmpty().Min();
				min = Math.Abs(min) > int.MaxValue ? 0 : (int) min;
				return min;
			}
			catch (Exception e)
			{
				return 0;
			}
		}

		public static decimal RemoveNonDigits(string _string)
		{
			if (string.IsNullOrEmpty(_string) || string.IsNullOrWhiteSpace(_string)) return default(decimal);

			var digitsOnly = new Regex(@"[^\d.,]");

			var res = digitsOnly.Replace(_string, "");
			if (string.IsNullOrEmpty(res) || string.IsNullOrWhiteSpace(res)) return 0;

			if (res.LastOrDefault() == '.' || res.LastOrDefault() == ',') res = res.Substring(0, res.Length - 1);

			if (res.Contains(",") || res.Contains("."))
			{
				res = res.Replace(',', '.');
				var separated = res.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
				if (separated.Length == 0) return 0;
				if (separated.Last().Length == 2) res = res.Substring(0, res.Length - 2);
				res = res.Replace(".", "");
			}

			var success = TryParse(res, out var price);
			return success ? price : 0;
		}

		private void LoadLinks()
		{
			var links = OpenJson<Dictionary<string, string[]>>("Open links");
			try
			{
				_links = links.ToDictionary(k => k.Key, v => v.Value.Select(s => new Uri(s)).ToArray());
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				throw;
			}
		}

		private void LoadConfig()
		{
			try
			{
				_config = OpenJson<Config>("Open config");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				throw;
			}
		}

        /// <summary>
        /// Запрашивает JSON файл и десериализует в нужный тип
        /// </summary>
        /// <typeparam name="TData"></typeparam>
        /// <param name="title"></param>
        /// <returns></returns>
		private TData OpenJson<TData>(string title)
		{
			try
			{
				// Диалог
				var fileDialog = new OpenFileDialog
				{
					Multiselect = false,
					Title = title,
					Filter = "Json files (*.json)|*.json" //JSON
				};
                
				if (fileDialog.ShowDialog() == DialogResult.OK)
				{
					//Десериализация
					var rawLinks = File.ReadAllText(fileDialog.FileName, Encoding.ASCII);
					var content = JsonConvert.DeserializeObject<TData>(rawLinks);
					return content;
				}

				Console.WriteLine("Error in config dialog");
				throw new ArgumentException("Error in dialog");
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error OpenJson() {ex}");
				throw;
			}
		}

        /// <summary>
        /// Определение пути, куда сохранится эксель
        /// </summary>
		private void DefineSavePath()
		{
			try
			{
				var sfd = new SaveFileDialog
				{
					Title = "Save file",
					Filter = "Книга Excel|*.xlsx"
				};
				sfd.ShowDialog();
				_savePath = sfd.FileName;
			}
			catch (Exception)
			{
				Console.WriteLine("Error defining save path");
				throw;
			}
		}

		private void SetCurrencies()
		{
			try
			{
                // Парсим валюты из сети
                ParseCurrencies();

                // Объединяем валюты их конфига с внешними значениями. В приоритете значения из конфига
                var configCurrency = _config.Currencies.ToDictionary(x => x.Name, x => new Currency {Value = x.Value});

                _currencies.Valute = configCurrency
                    .Union(_currencies.Valute, new PreserveSourceComparer<string, Currency>())
                    .ToDictionary(x => x.Key, x => x.Value);
            }
            catch (Exception ex)
			{
				Console.WriteLine($"Exception config : {ex.Message}");
				throw;
			}
		}


		private static WebClient GetWebClient()
		{
            var client = new WebClient {Encoding = Encoding.UTF8};
            client.Headers.Add("Accept: text/html, application/xhtml+xml, application/pdf, */*");
			client.Headers.Add("User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
			client.Headers.Add("Accept-Language: en-US,en;q=0.9");
			client.Headers.Add("Cache-Control: no-cache");
			client.Headers.Add("Upgrade-Insecure-Requests: 1");
			return client;
		}

		private void ParseCurrencies()
		{
			var currenciesApi = "https://www.cbr-xml-daily.ru/daily_json.js";
			string loadedJson;

			using (var webClient = new WebClient())
			{
				loadedJson = webClient.DownloadString(currenciesApi);
			}

			_currencies = JsonConvert.DeserializeObject<Currencies>(loadedJson);
            _currencies.Valute.Add("RUB", new Currency {Value = 1});
		}

        private class PreserveSourceComparer<TKey, TData> : IEqualityComparer<KeyValuePair<TKey, TData>>
        {
            public bool Equals(KeyValuePair<TKey, TData> x, KeyValuePair<TKey, TData> y)
            {
                return x.Key.Equals(y.Key);
            }

            public int GetHashCode(KeyValuePair<TKey, TData> obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}