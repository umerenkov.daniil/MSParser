﻿using System;

namespace MSParser
{
	class Program
	{
        [STAThread]
		static void Main(string[] args)
		{
            // Точка входа
            // STAThread (single-thread) нужно для взаимодействия с UI-элементами винды (диалоговое окно)
            new Parser().Run();
            
			Console.ReadLine();
		}
    }
}
